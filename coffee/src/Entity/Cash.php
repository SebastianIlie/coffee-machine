<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cash")
 */
class Cash
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @var string
     * @ORM\Column(
     *     name="value",
     *     type="string",
     *     length=4,
     *     nullable=false,
     *     options={"comment": "Selling price when the order was placed"}
     * )
     */
    private string $value;

    /**
     * @var int
     * @ORM\Column(
     *     name="quantity",
     *     type="smallint",
     *     nullable=false
     * )
     */
    private int $quantity;

    /**
     * @var \DateTime
     * @ORM\Column(
     *     name="created_at",
     *     type="datetime",
     *     options={"default": "CURRENT_TIMESTAMP"}
     * )
     */
    private \DateTime $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(
     *     name="updated_at",
     *     type="datetime",
     *     columnDefinition="DATETIME on update CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP"
     * )
     */
    private \DateTime $updatedAt;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Cash
     */
    public function setId(?int $id): Cash
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return Cash
     */
    public function setValue(string $value): Cash
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return Cash
     */
    public function setQuantity(int $quantity): Cash
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return Cash
     */
    public function setCreatedAt(\DateTime $createdAt): Cash
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return Cash
     */
    public function setUpdatedAt(\DateTime $updatedAt): Cash
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}