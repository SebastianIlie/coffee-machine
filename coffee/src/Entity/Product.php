<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="product")
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @var string
     * @ORM\Column(
     *     name="name",
     *     type="string",
     *     length=40,
     *     unique=true
     * )
     */
    private string $name;

    /**
     * @var string
     * @ORM\Column(
     *     name="selling_price",
     *     type="string",
     *     length=7
     * )
     */
    private string $sellingPrice;

    /**
     * @var \DateTime
     * @ORM\Column(
     *     name="created_at",
     *     type="datetime",
     *     options={"default": "CURRENT_TIMESTAMP"}
     * )
     */
    private \DateTime $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(
     *     name="updated_at",
     *     type="datetime",
     *     columnDefinition="DATETIME on update CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP"
     * )
     */
    private \DateTime $updatedAt;

    /**
     * @var Collection<ProductComponent>
     * @ORM\OneToMany(targetEntity="ProductComponent", mappedBy="product")
     */
    private Collection $components;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Product
     */
    public function setId(?int $id): Product
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Product
     */
    public function setName(string $name): Product
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getSellingPrice(): string
    {
        return $this->sellingPrice;
    }

    /**
     * @param string $sellingPrice
     * @return Product
     */
    public function setSellingPrice(string $sellingPrice): Product
    {
        $this->sellingPrice = $sellingPrice;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return Product
     */
    public function setCreatedAt(\DateTime $createdAt): Product
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return Product
     */
    public function setUpdatedAt(\DateTime $updatedAt): Product
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getComponents(): Collection
    {
        return $this->components;
    }

    /**
     * @param Collection $components
     * @return Product
     */
    public function setComponents(Collection $components): Product
    {
        $this->components = $components;
        return $this;
    }
}
