<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="order_payment")
 */
class OrderPayment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @var Order
     * @ORM\OneToOne(targetEntity="Order")
     */
    private Order $order;

    /**
     * @var string
     * @ORM\Column(type="string", length=10, nullable=false)
     */
    private string $type;

    /**
     * @var string
     * @ORM\Column(type="string", length=7, nullable=false)
     */
    private string $sum;

    /**
     * @var \DateTime
     * @ORM\Column(
     *     name="created_at",
     *     type="datetime",
     *     options={"default": "CURRENT_TIMESTAMP"}
     * )
     */
    private \DateTime $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(
     *     name="updated_at",
     *     type="datetime",
     *     columnDefinition="DATETIME on update CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP"
     * )
     */
    private \DateTime $updatedAt;

    /**
     * OrderPayment constructor.
     * @param Order $order
     * @param string $type
     * @param string $sum
     */
    public function __construct(Order $order, string $type, string $sum)
    {
        $this->order = $order;
        $this->type = $type;
        $this->sum = $sum;
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return OrderPayment
     */
    public function setId(?int $id): OrderPayment
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @param Order $order
     * @return OrderPayment
     */
    public function setOrder(Order $order): OrderPayment
    {
        $this->order = $order;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return OrderPayment
     */
    public function setType(string $type): OrderPayment
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getSum(): string
    {
        return $this->sum;
    }

    /**
     * @param string $sum
     * @return OrderPayment
     */
    public function setSum(string $sum): OrderPayment
    {
        $this->sum = $sum;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return OrderPayment
     */
    public function setCreatedAt(\DateTime $createdAt): OrderPayment
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return OrderPayment
     */
    public function setUpdatedAt(\DateTime $updatedAt): OrderPayment
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
