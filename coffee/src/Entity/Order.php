<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="`order`")
 */
class Order
{
    public const STATUS_PLACED = 0;
    public const STATUS_CONFIRMED = 1;
    public const STATUS_CANCELED = -1;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @var string
     * @ORM\Column(
     *     name="selling_price",
     *     type="string",
     *     length=7,
     *     nullable=false,
     *     options={"comment": "Selling price when the order was placed"}
     * )
     */
    private string $sellingPrice;

    /**
     * @var string
     * @ORM\Column(
     *     name="product_name",
     *     type="string",
     *     length=40,
     *     nullable=false
     * )
     */
    private string $productName;

    /**
     * @var OrderPayment
     * @ORM\OneToOne(targetEntity="OrderPayment")
     */
    private OrderPayment $payment;

    /**
     * @var \DateTime
     * @ORM\Column(
     *     name="created_at",
     *     type="datetime",
     *     options={"default": "CURRENT_TIMESTAMP"}
     * )
     */
    private \DateTime $createdAt;

    /**
     * @var int
     * @ORM\Column(type="smallint", name="status")
     */
    private int $status = self::STATUS_PLACED;

    public function __construct(string $sellingPrice, string $productName)
    {
        $this->sellingPrice = $sellingPrice;
        $this->productName = $productName;
        $this->createdAt = new \DateTime();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Order
     */
    public function setId(?int $id): Order
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getSellingPrice(): string
    {
        return $this->sellingPrice;
    }

    /**
     * @param string $sellingPrice
     * @return Order
     */
    public function setSellingPrice(string $sellingPrice): Order
    {
        $this->sellingPrice = $sellingPrice;
        return $this;
    }

    /**
     * @return string
     */
    public function getProductName(): string
    {
        return $this->productName;
    }

    /**
     * @param string $productName
     * @return Order
     */
    public function setProductName(string $productName): Order
    {
        $this->productName = $productName;
        return $this;
    }

    /**
     * @return OrderPayment
     */
    public function getPayment(): OrderPayment
    {
        return $this->payment;
    }

    /**
     * @param OrderPayment $payment
     * @return Order
     */
    public function setPayment(OrderPayment $payment): Order
    {
        $this->payment = $payment;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return Order
     */
    public function setCreatedAt(\DateTime $createdAt): Order
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Order
     */
    public function setStatus(int $status): Order
    {
        $this->status = $status;
        return $this;
    }

}
