<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class ProductComponent
 * @package App\Entity
 * @ORM\Entity()
 * @ORM\Table("product_components")
 */
class ProductComponent
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @var Product
     * @ORM\ManyToOne(targetEntity="Product", fetch="EAGER")
     */
    private Product $product;

    /**
     * @var Component
     * @ORM\ManyToOne(targetEntity="Component", fetch="EAGER")
     *
     */
    private Component $component;

    /**
     * @var string
     * @ORM\Column(
     *     name="component_requirement",
     *     type="string",
     *     length=6,
     *     nullable=false
     * )
     */
    private string $componentRequirement;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return ProductComponent
     */
    public function setId(?int $id): ProductComponent
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @param Product $product
     * @return ProductComponent
     */
    public function setProduct(Product $product): ProductComponent
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @return Component
     */
    public function getComponent(): Component
    {
        return $this->component;
    }

    /**
     * @param Component $component
     * @return ProductComponent
     */
    public function setComponent(Component $component): ProductComponent
    {
        $this->component = $component;
        return $this;
    }

    /**
     * @return string
     */
    public function getComponentRequirement(): string
    {
        return $this->componentRequirement;
    }

    /**
     * @param string $componentRequirement
     * @return ProductComponent
     */
    public function setComponentRequirement(string $componentRequirement): ProductComponent
    {
        $this->componentRequirement = $componentRequirement;
        return $this;
    }
}