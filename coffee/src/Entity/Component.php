<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="component",
 *     indexes={
 *          @ORM\Index(name="IDX_AVAILABLE_QTY", columns={"available_qty"})
 *     })
 */
class Component
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @var string
     * @ORM\Column(
     *     name="name",
     *     type="string",
     *     length=40,
     *     nullable=false
     * )
     */
    private string $name;

    /**
     * @var string
     * @ORM\Column(
     *     name="unit_of_measure",
     *     type="string",
     *     length=7,
     *     nullable=false
     * )
     */
    private string $unitOfMeasure;

    /**
     * @var string
     * @ORM\Column(
     *     name="available_qty",
     *     type="string",
     *     length=6,
     *     nullable=false
     * )
     */
    private string $availableQty;

    /**
     * @var int
     * @ORM\Column(name="priority", type="smallint", nullable=false)
     */
    private int $priority;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="ProductComponent", mappedBy="component")
     */
    private Collection $productComponents;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getUnitOfMeasure(): string
    {
        return $this->unitOfMeasure;
    }

    /**
     * @param string $unitOfMeasure
     */
    public function setUnitOfMeasure(string $unitOfMeasure): void
    {
        $this->unitOfMeasure = $unitOfMeasure;
    }

    /**
     * @return string
     */
    public function getAvailableQty(): string
    {
        return $this->availableQty;
    }

    /**
     * @param string $availableQty
     */
    public function setAvailableQty(string $availableQty): void
    {
        $this->availableQty = $availableQty;
    }

    /**
     * @param string $desiredQty
     * @return bool
     */
    public function hasAvailableQty(string $desiredQty): bool
    {
        return bccomp($this->availableQty, $desiredQty) >= 0;
    }
}