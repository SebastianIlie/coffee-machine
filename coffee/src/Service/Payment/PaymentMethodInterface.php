<?php
declare(strict_types=1);

namespace App\Service\Payment;

use App\Entity\Order;

interface PaymentMethodInterface
{
    /**
     * @param Order $order
     * @param array $paymentData
     * @return mixed
     */
    public function pay(Order $order, array $paymentData): void;
}
