<?php
declare(strict_types=1);

namespace App\Service\Payment;

use App\Entity\Order;
use App\Entity\OrderPayment;
use App\Exception\ChangeUnavailableException;
use Doctrine\ORM\EntityManagerInterface;

class PaymentService
{
    /**
     * @var array<PaymentMethodInterface>
     */
    private array $availablePaymentMethods = [];

    /**
     * @var PaymentMethodInterface
     */
    private PaymentMethodInterface $processingStrategy;

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * @required
     * @param EntityManagerInterface $entityManager
     * @return PaymentService
     */
    public function setEntityManager(EntityManagerInterface $entityManager): PaymentService
    {
        $this->entityManager = $entityManager;
        return $this;
    }

    /**
     * @param PaymentMethodInterface $paymentMethod
     * @return PaymentService
     */
    public function addAvailablePaymentMethod(PaymentMethodInterface $paymentMethod): PaymentService
    {
        $this->availablePaymentMethods[] = $paymentMethod;

        return $this;
    }

    /**
     * @param Order $order
     * @param array $paymentData
     * @return OrderPayment
     */
    public function process(Order $order, array $paymentData): OrderPayment
    {
        try {
            $this->processingStrategy->pay($order, $paymentData);
            return $this->newPayment($order);
        } catch (ChangeUnavailableException $exception) {
            $this->triggerRefund($order);
        } catch (\Throwable $exception) {
            //process retries, cancel, whatever the case
        }
    }

    /**
     * @param Order $order
     * @return OrderPayment
     */
    private function newPayment(Order $order): OrderPayment
    {
        $payment = (new OrderPayment($order, $this->processingStrategy::CODE, $order->getSellingPrice()));
        $this->entityManager->persist($payment);
        return $payment;
    }

    /**
     * @return array
     */
    public function getAvailablePaymentMethods(): array
    {
        return $this->availablePaymentMethods;
    }

    /**
     * @param PaymentMethodInterface $paymentMethod
     */
    public function setProcessingStrategy(PaymentMethodInterface $paymentMethod): void
    {
        $this->processingStrategy = $paymentMethod;
    }

    private function triggerRefund(Order $order): void
    {
        //@TODO implement
    }
}
