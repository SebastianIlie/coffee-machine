<?php
declare(strict_types=1);

namespace App\Service\Payment;

use App\Entity\Order;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class Card implements OnlinePaymentMethodInterface
{
    public const CODE = 'card';

    private HttpClientInterface $httpClient;

    /**
     * @required
     * @param HttpClientInterface $httpClient
     * @return Card
     */
    public function setHttpClient(HttpClientInterface $httpClient): Card
    {
        $this->httpClient = $httpClient;
        return $this;
    }

    public function pay(Order $order, array $paymentData): void
    {
        $this->contactProcessor($order);
    }

    public function contactProcessor(Order $order): void
    {
        //$this->httpClient->request();
    }
}
