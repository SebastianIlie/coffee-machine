<?php
declare(strict_types=1);

namespace App\Service\Payment;

use App\Entity\Order;

interface OnlinePaymentMethodInterface extends PaymentMethodInterface
{
    /**
     * @param Order $order
     */
    public function contactProcessor(Order $order): void;
}