<?php
declare(strict_types=1);

namespace App\Service\Payment;

use App\Entity\Cash as CashEntity;
use App\Entity\Order;
use App\Exception\ChangeUnavailableException;
use App\Repository\CashRepository;

class Cash implements PaymentMethodInterface
{
    public const CODE = 'cash';
    public const ACCEPTED_MISSING_CHANGE = '1';

    private CashRepository $cashRepo;

    /**
     * @required
     * @param CashRepository $cashRepo
     * @return Cash
     */
    public function setCashRepo(CashRepository $cashRepo): Cash
    {
        $this->cashRepo = $cashRepo;
        return $this;
    }

    /**
     * @param Order $order
     * @param array $paymentData
     * @throws ChangeUnavailableException
     */
    public function pay(Order $order, array $paymentData): void
    {
        $this->checkAvailableChange($order, $paymentData);
        //i don't normally like symfony events but i've spent enough time on this test
        //i'd use a even here in case change was required
    }

    /**
     * @param Order $order
     * @param array $paymentData
     * @throws ChangeUnavailableException
     */
    private function checkAvailableChange(Order $order, array $paymentData): void
    {
        $requiredChange = bcsub($paymentData['inserted_cash'], $order->getSellingPrice(), 2);

        if (1 === bccomp($requiredChange, '0.00', 2)) {
            $availableCash = $this->cashRepo->findAllAndSort('DESC');
            if (empty($availableCash)) {
                throw new ChangeUnavailableException();
            }
            /** @var CashEntity $monetaryUnit */
            foreach ($availableCash as $monetaryUnit) {
                $modulus = bcmod($requiredChange, $monetaryUnit->getValue(), 2);
                $div = bcdiv($requiredChange, $monetaryUnit->getValue());
                $quantityAvailable = $div <= $monetaryUnit->getQuantity();
                if ($quantityAvailable && (int)$div > 0) {
                    $monetaryUnit->setQuantity($monetaryUnit->getQuantity() - $div);
                    if (0 === bccomp($modulus, '0.00', 2)) {
                        break;
                    }
                    $requiredChange = bcsub(
                        $requiredChange,
                        bcmod($monetaryUnit->getValue(), $div, 2),
                        2
                    );
                };
            }

            if (1 === bccomp($modulus,self::ACCEPTED_MISSING_CHANGE)) {
                throw new ChangeUnavailableException();
            }
        }
    }
}
