<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\Order;
use App\Entity\OrderPayment;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;

class OrderService
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * @required
     * @param EntityManagerInterface $entityManager
     * @return OrderService
     */
    public function setEntityManager(EntityManagerInterface $entityManager): OrderService
    {
        $this->entityManager = $entityManager;
        return $this;
    }

    public function initOrder(Product $product): Order
    {
        $order = (new Order($product->getSellingPrice(), $product->getName()));
        $this->entityManager->persist($order);

        return $order;
    }

    public function place(Order $order, OrderPayment $orderPayment): void
    {
        $order->setPayment($orderPayment);
        $orderPayment->setOrder($order);
        $order->setStatus(Order::STATUS_CONFIRMED);
    }
}
