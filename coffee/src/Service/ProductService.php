<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\Product;
use App\Entity\ProductComponent;
use App\Repository\ProductComponentsRepository;
use Doctrine\ORM\EntityManagerInterface;

class ProductService
{
    private ProductComponentsRepository $prodComponentsRepo;

    /** @var EntityManagerInterface */
    private EntityManagerInterface $entityManager;

    /**
     * @required
     * @param ProductComponentsRepository $prodComponentsRepo
     * @return ProductService
     */
    public function setProdComponentsRepo(ProductComponentsRepository $prodComponentsRepo): ProductService
    {
        $this->prodComponentsRepo = $prodComponentsRepo;
        return $this;
    }

    /**
     * @required
     * @param EntityManagerInterface $entityManager
     * @return ProductService
     */
    public function setEntityManager(EntityManagerInterface $entityManager): ProductService
    {
        $this->entityManager = $entityManager;
        return $this;
    }

    public function getAvailableProducts(): array
    {
        $productComponents = $this->prodComponentsRepo->findAll();
        $availableProducts = [];

        /** @var ProductComponent $productComponent */
        foreach ($productComponents as $productComponent) {
            $this->updateProductData($productComponent, $availableProducts);
        }

        return $availableProducts;
    }

    /**
     * @param ProductComponent $productComponent
     * @return bool
     */
    private function validateAvailability(ProductComponent $productComponent): bool
    {
        return $productComponent->getComponent()->hasAvailableQty(
            $productComponent->getComponentRequirement()
        );
    }

    private function updateProductData(ProductComponent $productComponent, array &$availableProducts): void
    {
        $product = $productComponent->getProduct();
        //easier to just set relations here since doctrine does not
        $product->getComponents()->add($productComponent);
        if (!($productAvailable = $this->validateAvailability($productComponent))) {
            if (isset($availableProducts[$product->getId()])) {
                $availableProducts[$product->getId()]['available'] = false;
                return;
            }
        }

        if (!isset($availableProducts[$product->getId()])) {
            $availableProducts[$product->getId()] = [
                'product' => $product,
                'composition'=> '',
                'available' => $productAvailable
            ];
        }

        $component = $productComponent->getComponent();
        $availableProducts[$product->getId()]['composition'] .= sprintf(
            '%s %s %s ',
            $component->getName(),
            $productComponent->getComponentRequirement(),
            $component->getUnitOfMeasure(),
        );
    }

    public function updateProductAvailability(Product $product): void
    {
        /** @var ProductComponent $productComponent */
        foreach ($product->getComponents() as $productComponent) {
            $component = $productComponent->getComponent();
            $component->setAvailableQty(
                bcsub(
                    $component->getAvailableQty(),
                    $productComponent->getComponentRequirement(),
                    2
                )
            );
        }

        $this->entityManager->flush();
    }
}
