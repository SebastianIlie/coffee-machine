<?php
declare(strict_types=1);

namespace App\Exception;

use Throwable;

class ChangeUnavailableException extends \Exception
{
    public function __construct($message = 'Change unavailable', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}