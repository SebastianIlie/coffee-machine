<?php
declare(strict_types=1);

namespace App\Exception;

class NotEnoughChangeException extends \Exception
{
    public function __construct($message = 'Not enough change available', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}