<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Cash;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CashRepository extends ServiceEntityRepository
{
    /**
     * ProductComponentsRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Cash::class);
    }

    /**
     * @param string $sort
     * @return array
     */
    public function findAllAndSort(string $sort): array
    {
        return $this->findBy([], array('value' => $sort));
    }
}
