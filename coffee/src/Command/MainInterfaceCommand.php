<?php
declare(strict_types=1);

namespace App\Command;

use App\Entity\Order;
use App\Entity\OrderPayment;
use App\Entity\Product;
use App\Exception\ChangeUnavailableException;
use App\Exception\NotEnoughChangeException;
use App\Service\OrderService;
use App\Service\Payment\PaymentMethodInterface;
use App\Service\Payment\PaymentService;
use App\Service\ProductService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;

class MainInterfaceCommand extends Command
{
    use LockableTrait;

    /** @var ProductService */
    private ProductService $productService;

    /** @var OrderService */
    private OrderService $orderService;

    /** @var PaymentService */
    private PaymentService $paymentService;

    /**
     * MainInterfaceCommand constructor.
     * @param ProductService $listingService
     * @param OrderService $orderService
     * @param PaymentService $paymentService
     */
    public function __construct(
        ProductService $productService,
        OrderService $orderService,
        PaymentService $paymentService
    )
    {
        parent::__construct('app:main:interface');
        $this->productService = $productService;
        $this->orderService = $orderService;
        $this->paymentService = $paymentService;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return Command::FAILURE;
        }
        $availableProducts = $this->productService->getAvailableProducts();
        $productChoices = $this->getProductChoices($availableProducts);
        if (empty($productChoices)) {
            $output->writeln('Sorry, there are no products available');
            $this->release();

            return Command::SUCCESS;
        }

        $desiredProduct = $this->askForDesiredProduct($output, $input, $productChoices);
        $this->askForPaymentMethod($output, $input);
        //something is going on with symfony input/output here and i dont have time to check what
        $products = array_values($availableProducts);
        $order = $this->orderService->initOrder($products[$desiredProduct]['product']);
        $payment = $this->askForPaymentInput($output, $input, $order);
        $this->orderService->place($order, $payment);

        $this->productService->updateProductAvailability($products[$desiredProduct]['product']);
        $this->release();

        return Command::SUCCESS;
    }

    /**
     * @param OutputInterface $output
     * @param InputInterface $input
     * @param Order $order
     * @return OrderPayment
     */
    private function askForPaymentInput(
        OutputInterface $output,
        InputInterface $input,
        Order $order
    ): OrderPayment {
        $helper = $this->getHelper('question');
        $question = new Question('Please input required sum or higher ' . $order->getSellingPrice() .':');

        $response = $helper->ask($input, $output, $question);
        /**
         * this should have gone to a cash/card device normally but emulating it is kind of pointless
         * the cash part would return bills/coins inserted, the card would return card details (probably)
         * if however i were to emulate/integrate with it i would not do it here but in different services
         */
        return $this->paymentService->process($order, ['inserted_cash' => $response]);
    }

    private function getProductChoices(array $availableProducts): array
    {
        $productChoices = [];

        foreach ($availableProducts as $availableProduct) {
            $productChoices[$availableProduct['product']->getId()] = $this->productOutputFormat($availableProduct);
        }

        return $productChoices;
    }

    /**
     * @TODO maybe move ask* functions to a chain
     * @param OutputInterface $output
     * @param InputInterface $input
     */
    private function askForPaymentMethod(OutputInterface $output, InputInterface $input): void
    {
        $availableMethods = $this->paymentService->getAvailablePaymentMethods();
        $choices = [];
        $i = 0;
        foreach ($this->paymentService->getAvailablePaymentMethods() as $paymentMethod) {
            $choices[$paymentMethod::CODE] = $i++;
        }
        $helper = $this->getHelper('question');
        $question = new ChoiceQuestion(
            'Please select desired payment method',
            $choices
        );
        $question->setErrorMessage('Invalid payment choice');

        $response = $helper->ask($input, $output, $question);
        $this->paymentService->setProcessingStrategy($availableMethods[$choices[$response]]);
    }

    /**
     * @param OutputInterface $output
     * @param InputInterface $input
     * @param array $choices
     * @return int
     */
    private function askForDesiredProduct(OutputInterface $output, InputInterface $input, array $choices): int
    {
        $helper = $this->getHelper('question');
        $question = new ChoiceQuestion('Please input desired product', $choices);
        $question->setErrorMessage('Invalid product choice');
        return (int)$helper->ask($input, $output, $question);
    }

    /**
     * //@TODO this is just one method and i'm not really into creating classes for 1 method if it's not reusable
     * @param array $productInfo
     * @return array
     */
    private function productOutputFormat(array $productInfo): string
    {
        /** @var Product $product */
        $product = $productInfo['product'];
        return sprintf(
            'Name: %s Price: %s Composition: %s Available: %s',
            $product->getName(),
            $product->getSellingPrice(),
            $productInfo['composition'],
            true === $productInfo['available'] ? 'Yes' : 'No'
        );
    }
}